package com.pos.printer;

import java.awt.print.PrinterException;
import java.io.*;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.annotation.MultipartConfig;

@RestController
@MultipartConfig(fileSizeThreshold = 20971520)
public class PrinterController {

  @RequestMapping("/open-drawer")
  public boolean openCashDrawer() {
    PrinterService printerService = new PrinterService();

    byte[] openDrawer = new byte[]  {(byte)27, (byte)112, (byte)0, (byte)25, (byte)250};

    printerService.printBytes("zebra", openDrawer);

    return true;
  }

  @PostMapping("/print")
  public String print(@RequestParam("file") MultipartFile file,
                                 RedirectAttributes redirectAttributes) {
    if (file.isEmpty()) {
      redirectAttributes.addFlashAttribute("message", "Please select a file to upload");
      return "redirect:uploadStatus";
    }

    try {
      InputStream inputStream =  new BufferedInputStream(file.getInputStream());
      PrinterService printerService = new PrinterService();
      printerService.printPdf("zebra", inputStream);

    } catch (IOException | PrinterException e) {
      e.printStackTrace();
    }

    return "printed";
  }
}
