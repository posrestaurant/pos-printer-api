package com.pos.printer;

import org.apache.pdfbox.io.IOUtils;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.printing.PDFPageable;

import javax.print.*;
import javax.print.attribute.HashPrintRequestAttributeSet;
import javax.print.attribute.PrintRequestAttributeSet;
import java.awt.*;
import java.awt.print.PageFormat;
import java.awt.print.Printable;
import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;
import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by jxu on 3/4/18.
 */
public class PrinterService implements Printable {

  public List<String> getPrinters() {

    DocFlavor flavor = DocFlavor.BYTE_ARRAY.AUTOSENSE;
    PrintRequestAttributeSet pras = new HashPrintRequestAttributeSet();

    PrintService printServices[] = PrintServiceLookup.lookupPrintServices(
      flavor, pras);

    List<String> printerList = new ArrayList<String>();
    for (PrintService printerService : printServices) {
      printerList.add(printerService.getName());
    }

    return printerList;
  }

  @Override
  public int print(Graphics g, PageFormat pf, int page)
    throws PrinterException {
    if (page > 0) { /* We have only one page, and 'page' is zero-based */
      return NO_SUCH_PAGE;
    }

		/*
     * User (0,0) is typically outside the imageable area, so we must
		 * translate by the X and Y values in the PageFormat to avoid clipping
		 * */
    Graphics2D g2d = (Graphics2D) g;
    g2d.translate(pf.getImageableX(), pf.getImageableY());
    /* Now we perform our rendering */

    g.setFont(new Font("Roman", 0, 8));
    g.drawString("Hello world !", 0, 10);

    return PAGE_EXISTS;
  }

  public void printPdf(String printerName, InputStream inputStream) throws IOException, PrinterException {

    // find the printService of name printerName
    DocFlavor flavor = DocFlavor.BYTE_ARRAY.AUTOSENSE;
    PrintRequestAttributeSet pras = new HashPrintRequestAttributeSet();

    PrintService printService[] = PrintServiceLookup.lookupPrintServices(flavor, pras);
    PrintService service = findPrintService(printerName, printService);
    PrinterJob job = PrinterJob.getPrinterJob();
    job.setPageable(new PDFPageable(PDDocument.load(inputStream)));
    job.setPrintService(service);

    try {
      job.print();
    } catch (Exception e) {
      // TrODO Auto-generated catch block
      e.printStackTrace();
    }
  }

  public void printBytes(String printerName, byte[] bytes) {

    DocFlavor flavor = DocFlavor.BYTE_ARRAY.AUTOSENSE;
    PrintRequestAttributeSet pras = new HashPrintRequestAttributeSet();

    PrintService printService[] = PrintServiceLookup.lookupPrintServices(
      flavor, pras);
    PrintService service = findPrintService(printerName, printService);

    DocPrintJob job = service.createPrintJob();

    try {

      Doc doc = new SimpleDoc(bytes, flavor, null);

      job.print(doc, null);

    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  private PrintService findPrintService(String printerName,
                                        PrintService[] services) {
    for (PrintService service : services) {
      if (service.getName().equalsIgnoreCase(printerName)) {
        return service;
      }
    }

    return null;
  }
}
